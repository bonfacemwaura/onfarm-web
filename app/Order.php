<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{
    protected $fillable = ['total_price','delivered'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function orderItems() {
        return $this->belongsToMany(Product::class)->withPivot('quantity','total');
    }
}
