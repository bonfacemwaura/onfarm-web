<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Filters\ProductFilter;
use App\Product;

class FrontEndController extends Controller
{

    public function index()
    {
        $categories = Category::take(4)->get();
        $products = Product::take(8)->get();

        return view('front.home')
            ->withCategories($categories)
            ->withProducts($products);
    }

    public function products(ProductFilter $filter)
    {
        $categories = Category::all();

        $products = Product::filter($filter)->paginate(12);

        return view('front.products')
            ->withProducts($products)
            ->withCategories($categories);
    }

    public function product(Product $product)
    {
        return view('front.product', compact('product'));
    }
}
