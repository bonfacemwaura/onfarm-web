<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wishlist;
use Auth;

class wishlistController extends Controller
{
    public function __construct() {
        $this->middleware(['auth']);
    }
    public function index()
    {
      $user = Auth::user();
      $wishlists = Wishlist::where("user_id", "=", $user->id)->orderby('id', 'desc')->paginate(10);
      return view('front.wishlist', compact('user', 'wishlists'));
    }
    public function store(Request $request)
    {
$this->validate($request, array(
 'user_id'=>'required',
 'product_id' =>'required',
));

$status=Wishlist::where('user_id',Auth::user()->id)
->where('product_id',$request->product_id)
->first();

if(isset($status->user_id) and isset($request->product_id))
   {
       return redirect()->back()->with('flash_messaged', 'This item is already in your 
       wishlist!');
   }
   else
   {
       $wishlist = new Wishlist;

       $wishlist->user_id = $request->user_id;
       $wishlist->product_id = $request->product_id;
       $wishlist->save();

       return redirect()->back()->with('flash_message',
                     'Item, '. $wishlist->product->title.' Added to your wishlist.');
   }

}
public function destroy($id)
    {
      $wishlist = Wishlist::findOrFail($id);
      $wishlist->delete();

      return redirect()->route('wishlist.index')
          ->with('flash_message',
           'Item successfully deleted');
    }
}
