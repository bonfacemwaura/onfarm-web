<?php 
namespace App\Http\Filters;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class ProductFilter extends QueryFilter
{
    public function name(string $name) {
        $this->builder->where('name', 'like','%' . $name . '%');
    }

    public function category(int $category)
    {
        $this->builder->where('category_id', $category);
    }

    public function quantity(int $quantity)
    {
        $this->builder->where('quantity', '>=', $quantity);
    }

    public function readyDate(string $readyDate)
    {
        $this->builder->where('ready_date', '>=', Carbon::parse($readyDate));
    }
}