<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class ProductReview extends Model
{
    protected $fillable=[
        'headline',
        'description',
        'product_id',
        'rating'
    ];
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}

