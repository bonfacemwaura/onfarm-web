<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Filters\Filterable;
use App\Category;
use App\Rating;
use App\User;
use App\Wishlist;

class Product extends Model
{
    use Filterable;

    protected $guarded = [];
    protected $dates = ['ready_date'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function reviews()
    {
        return $this->hasMany(ProductReview::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function wishlist()
    {
        return $this->hasMany(Wishlist::class);
    }
    public function getStarRating()
    {
        $count = $this->reviews()->count();
        if(empty($count)){
            return 0;
        }
        $starCountSum=$this->reviews()->sum('rating');
        $average=$starCountSum/ $count;

       return $average;

    }

}

