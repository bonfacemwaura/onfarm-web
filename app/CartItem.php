<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model

{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->hasOne(Product::class);
    }
}
