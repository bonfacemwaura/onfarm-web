<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontEndController@index')->name('front.index');
Route::get('product/{product}', 'FrontEndController@product')->name('product');
Route::get('products', 'FrontEndController@products')->name('front.products');

Auth::routes();

Route::group(['prefix' => 'seller', 'middleware' => 'auth'], function () {
    Route::post('toggledeliver/{orderId}', 'OrderController@toggledeliver')->name('toggle.deliver');
    Route::get('/', function () {
        return view('admin.index');
    })->name('admin.index');
    Route::post('product/image-upload/{productId}', 'ProductsController@uploadImages');
    Route::resource('product', 'ProductsController');
    Route::resource('category', 'CategoriesController');
    Route::get('orders/{type?}', 'OrderController@Orders');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('cart', 'ProductsController@cart')->name('cart');
    Route::get('add-to-cart/{id}', 'ProductsController@addToCart');
    Route::patch('update-cart', 'ProductsController@updating');
    Route::delete('remove-from-cart', 'ProductsController@remove');
    Route::resource('review','ProductReviewController');
});

Route::get('wishlist', 'WishlistController@store')->name('addtowishlist');
Route::post('make-order', 'ProductsController@createOrder')->name('make.order');
