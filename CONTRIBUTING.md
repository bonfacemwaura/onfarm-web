<div>

Laravel is used by many thousands of developers every day to create all kinds of websites and applications. But fortunately, there are some very commonly used naming conventions that are followed by many developers when naming their Laravel project's variables, methods and functions. Here is my overview of the best naming conventions for Laravel.

<div class="table_of_contents">

#### Laravel Naming Conventions Table of contents

*   [Naming Controllers](#section_naming-controllers)
*   [Naming database tables in Laravel](#section_naming-database-tables-in-laravel)
    1.  [Pivot tables](#section_pivot-tables)
    2.  [Table columns names](#section_table-columns-names)
    3.  [Primary Key](#section_primary-key)
    4.  [Foreign Keys](#section_foreign-keys)
*   [Variables](#section_variables)
*   [Naming Conventions for models](#section_naming-conventions-for-models)
    1.  [Naming Models in Laravel](#section_naming-models-in-laravel)
    2.  [Model properties](#section_model-properties)
    3.  [Model Methods](#section_model-methods)
    4.  [Relationships](#section_relationships)
        1.  [hasOne or belongsTo relationship (one to many)](#section_hasone-or-belongsto-relationship-one-to-many)
        2.  [hasMany, belongsToMany, hasManyThrough (one to many)](#section_hasmany-belongstomany-hasmanythrough-one-to-many)
        3.  [Polymorphic relationships](#section_polymorphic-relationships)
*   [Controllers](#section_controllers)
*   [Method naming conventions in controllers](#section_method-naming-conventions-in-controllers)
*   [Traits](#section_traits)
*   [Blade view files](#section_blade-view-files)

</div>

(Please let me know in a comment if you disagree with anything here)

### Naming Controllers

Controllers should be in singular case, no spacing between words, and end with "Controller".

Also, each word should be capitalised (i.e. BlogController, not blogcontroller).

For example: `BlogController`, `AuthController`, `UserController`.

<div class="alert alert-danger">Bad examples: `UsersController` (because it is in plural), `Users` (because it is missing the Controller suffix).</div>

### Naming database tables in Laravel

DB tables should be in lower case, with underscores to separate words (snake_case), and should be in plural form.

For example: `posts`, `project_tasks`, `uploaded_images`.

<div class="alert alert-danger">Bad examples: `all_posts`, `Posts`, `post`, `blogPosts`</div>

#### Pivot tables

Pivot tables should be all lower case, each model in alphabetical order, separated by an underscore (snake_case).

For example: `post_user`, `task_user` etc.

<div class="alert alert-danger">Bad examples: `users_posts`, `UsersPosts`.</div>

#### Table columns names

Table column names should be in lower case, and snake_case (underscores between words). You shouldn't reference the table name.

For example: `post_body`, `id`, `created_at`.

<div class="alert alert-danger">Bad examples: `blog_post_created_at`, `forum_thread_title`, `threadTitle`.</div>

#### Primary Key

This should normally be `id`.

#### Foreign Keys

Foreign keys should be the model name (singular), with '_id' appended to it (assuming the PK in the other table is 'id').

For example: `comment_id`, `user_id`.

### Variables

Normal variables should typically be in camelCase, with the first character lower case.

For example: `$users = ...`, `$bannedUsers = ...`.

<div class="alert alert-danger">Bad examples: `$all_banned_users = ...`, `$Users=...`.</div>

If the variable contains an array or collection of multiple items then the variable name should be in plural. Otherwise, it should be in singular form.

For example: `$users = User::all();` (as this will be a collection of multiple User objects), but `$user = User::first()` (as this is just one object)

### Naming Conventions for models

#### Naming Models in Laravel

A model should be in singular, no spacing between words, and capitalised.

For example: `User` (\App\User or \App\Models\User, etc), `ForumThread`, `Comment`.

<div class="alert alert-danger">Bad examples: `Users`, `ForumPosts`, `blogpost`, `blog_post`, `Blog_posts`.</div>

Generally, your models should be able to automatically work out what database table it should use from the following method:

<div class="codeoutput">`

1.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">    <span style="color: #009933; font-style: italic;">/**</span></div>

2.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #009933; font-style: italic;">     * Get the table associated with the model.</span></div>

3.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #009933; font-style: italic;">     *</span></div>

4.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #009933; font-style: italic;">     * @return string</span></div>

5.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #009933; font-style: italic;">     */</span></div>

6.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">    <span style="color: #000000; font-weight: bold;">public</span> <span style="color: #000000; font-weight: bold;">function</span> getTable<span style="color: #009900;">(</span><span style="color: #009900;">)</span></div>

7.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">    <span style="color: #009900;">{</span></div>

8.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">        <span style="color: #b1b100;">if</span> <span style="color: #009900;">(</span><span style="color: #339933;">!</span> <span style="color: #990000;">isset</span><span style="color: #009900;">(</span><span style="color: #000088;">$this</span><span style="color: #339933;">-></span><span style="color: #004000;">table</span><span style="color: #009900;">)</span><span style="color: #009900;">)</span> <span style="color: #009900;">{</span></div>

9.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">            <span style="color: #b1b100;">return</span> <span style="color: #990000;">str_replace</span><span style="color: #009900;">(</span></div>

10.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">                <span style="color: #0000ff;">'\\'</span><span style="color: #339933;">,</span> <span style="color: #0000ff;">''</span><span style="color: #339933;">,</span> Str<span style="color: #339933;">::</span><span style="color: #004000;">snake</span><span style="color: #009900;">(</span>Str<span style="color: #339933;">::</span><span style="color: #004000;">plural</span><span style="color: #009900;">(</span>class_basename<span style="color: #009900;">(</span><span style="color: #000088;">$this</span><span style="color: #009900;">)</span><span style="color: #009900;">)</span><span style="color: #009900;">)</span></div>

11.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">            <span style="color: #009900;">)</span><span style="color: #339933;">;</span></div>

12.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">        <span style="color: #009900;">}</span></div>

14.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">        <span style="color: #b1b100;">return</span> <span style="color: #000088;">$this</span><span style="color: #339933;">-></span><span style="color: #004000;">table</span><span style="color: #339933;">;</span></div>

15.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">    <span style="color: #009900;">}</span></div>

`</div>

But of course you can just set `$this->table` in your model. See the section below on table naming conventions.

I recommend that you create models and migrations at the same time by running `php artisan make:model -m ForumPost`. This will auto-generate the migration file (in this case, for a DB table name of 'forum_posts').

#### Model properties

These should be lower case, snake_case. They should also follow the same conventions as the table column names.

For example: `$this->updated_at`, `$this->title`.

<div class="alert alert-danger">Bad examples: `$this->UpdatedAt`, `$this->blogTitle`.</div>

#### Model Methods

Methods in your models in Laravel projects, like all methods in your Laravel projects, should be camelCase but the first character lower case.

For example: `public function get()`, `public function getAll()`.

<div class="alert alert-danger">Bad examples: `public function GetPosts()`, `public function get_posts()`.</div>

#### Relationships

##### hasOne or belongsTo relationship (one to many)

These should be singular form and follow the same naming conventions of normal model methods (camelCase, but with the first letter lower case)

For example: `public function postAuthor()`, `public function phone()`.

##### hasMany, belongsToMany, hasManyThrough (one to many)

These should be the same as the one to many naming conventions, however, it should be in plural.

For example: `public function comments()`, `public function roles()`.

##### Polymorphic relationships

These can be a bit awkward to get the naming correct.

Ideally, you want to be able to have a method such as this:

<div class="codeoutput">`

1.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #000000; font-weight: bold;">public</span> <span style="color: #000000; font-weight: bold;">function</span> category<span style="color: #009900;">(</span><span style="color: #009900;">)</span></div>

2.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #009900;">{</span></div>

3.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">    <span style="color: #b1b100;">return</span> <span style="color: #000088;">$this</span><span style="color: #339933;">-></span><span style="color: #004000;">morphMany</span><span style="color: #009900;">(</span><span style="color: #0000ff;">'App\Category'</span><span style="color: #339933;">,</span> <span style="color: #0000ff;">'categoryable'</span><span style="color: #009900;">)</span><span style="color: #339933;">;</span></div>

4.  <div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #009900;">}</span></div>

`</div>

And Laravel will by default assume that there is a categoryable_id and categoryable_type.

But you can use the other optional parameters for `morphMany` ( `public function morphMany($related, $name, $type = null, $id = null, $localKey = null)`) to change the defaults.

### Controllers

### Method naming conventions in controllers

These should follow the same rules as model methods. I.e. camelCase (first character lowercase).

In addition, for normal CRUD operations, they should use one of the following method names.

<table class="table">

<thead>

<tr>

<th>Verb</th>

<th>URI</th>

<th>Typical Method Name</th>

<th>Route Name</th>

</tr>

</thead>

<tbody>

<tr>

<td>GET</td>

<td>`/photos`</td>

<td>`index()`</td>

<td>photos.index</td>

</tr>

<tr>

<td>GET</td>

<td>`/photos/create`</td>

<td>`create()`</td>

<td>photos.create</td>

</tr>

<tr>

<td>POST</td>

<td>`/photos`</td>

<td>`store()`</td>

<td>photos.store</td>

</tr>

<tr>

<td>GET</td>

<td>`/photos/{photo}`</td>

<td>`show()`</td>

<td>photos.show</td>

</tr>

<tr>

<td>GET</td>

<td>`/photos/{photo}/edit`</td>

<td>`edit()`</td>

<td>photos.edit</td>

</tr>

<tr>

<td>PUT/PATCH</td>

<td>`/photos/{photo}`</td>

<td>`update()`</td>

<td>photos.update</td>

</tr>

<tr>

<td>DELETE</td>

<td>`/photos/{photo}`</td>

<td>`destroy()`</td>

<td>photos.destroy</td>

</tr>

</tbody>

</table>

### Traits

Traits should be be adjective words.

<div class="alert alert-success">For example: `Notifiable`, `Dispatchable`, etc.</div>

### Blade view files

Blade files should be in lower case, snake_case (underscore between words).

For example: `all.blade.php`, `all_posts.blade.php`, etc

</div>