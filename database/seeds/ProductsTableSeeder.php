<?php

use App\Category;
use App\Product;
use App\User;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get categories
        $fruitsCategory  = Category::where('name', 'Fruits')->first();
        $vegetablesCategory  = Category::where('name', 'Vegetables')->first();
        $animalProductsCategory  = Category::where('name', 'Animal Products')->first();
        $cerealsCategory  = Category::where('name', 'Cereals')->first();

        // Get sellers
        $sellers = User::whereHas('roles', function($q) {
            $q->where('name', 'seller');
        })->get();

        // Seed fruits
        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Lime (Ndimu)',
            'image' => 'lime.webp',
            'price' => 187.00,
            'ready_date' => '2019-11-05',
            'quantity' => 20,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Pawpaw',
            'image' => 'pawpaw.webp',
            'price' => 149.00,
            'ready_date' => '2019-11-05',
            'quantity' => 30,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Apples',
            'image' => 'apple.webp',
            'price' => 35.00,
            'ready_date' => '2019-11-05',
            'quantity' => 40,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Bananas',
            'image' => 'banana.webp',
            'price' => 120.00,
            'ready_date' => '2019-11-05',
            'quantity' => 50,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Lemon (Local)',
            'image' => 'lemon.webp',
            'price' => 130.00,
            'ready_date' => '2019-11-05',
            'quantity' => 60,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Passion Fruit',
            'image' => 'passion.webp',
            'price' => 200.00,
            'ready_date' => '2019-11-05',
            'quantity' => 50,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Blackberries',
            'image' => 'blackberry.webp',
            'price' => 400.00,
            'ready_date' => '2019-11-05',
            'quantity' => 40,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Oranges (Imported)',
            'image' => 'orange.webp',
            'price' => 25.00,
            'ready_date' => '2019-11-05',
            'quantity' => 30,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Grapes',
            'image' => 'grape.webp',
            'price' => 370.00,
            'ready_date' => '2019-11-05',
            'quantity' => 20,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Avocado',
            'image' => 'avocado.webp',
            'price' => 40.00,
            'ready_date' => '2019-11-05',
            'quantity' => 70,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Kiwi',
            'image' => 'kiwi.webp',
            'price' => 100.00,
            'ready_date' => '2019-11-05',
            'quantity' => 30,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Butternut',
            'image' => 'butternut.webp',
            'price' => 120.00,
            'ready_date' => '2019-11-05',
            'quantity' => 25,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Pineapple',
            'image' => 'pineapple.webp',
            'price' => 160.00,
            'ready_date' => '2019-11-05',
            'quantity' => 45,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Apple Mangoes',
            'image' => 'mango.webp',
            'price' => 150.00,
            'ready_date' => '2019-11-05',
            'quantity' => 80,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Oranges (local)',
            'image' => 'orange.webp',
            'price' => 120.00,
            'ready_date' => '2019-11-05',
            'quantity' => 65,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Sweet Bananas',
            'image' => 'banana.webp',
            'price' => 120.00,
            'ready_date' => '2019-11-05',
            'quantity' => 70,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Mangoes',
            'image' => 'mango.webp',
            'price' => 150.00,
            'ready_date' => '2019-11-05',
            'quantity' => 60,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Cherry Tomatoes',
            'image' => 'cherrytomato.webp',
            'price' => 150.00,
            'ready_date' => '2019-11-05',
            'quantity' => 25,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Lemon (Imported)',
            'image' => 'lemon.webp',
            'price' => 450.00,
            'ready_date' => '2019-11-05',
            'quantity' => 80,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Water Melon',
            'image' => 'watermelon.webp',
            'price' => 300.00,
            'ready_date' => '2019-11-05',
            'quantity' => 28,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Pears (Imported)',
            'image' => 'pear.webp',
            'price' => 60.00,
            'ready_date' => '2019-11-05',
            'quantity' => 30,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Raspberry',
            'image' => 'raspberry.webp',
            'price' => 300.00,
            'ready_date' => '2019-11-05',
            'quantity' => 55,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Sugarcane',
            'image' => 'sugarcane.webp',
            'price' => 120.00,
            'ready_date' => '2019-11-05',
            'quantity' => 40,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Sweet Melon',
            'image' => 'sweetmelon.webp',
            'price' => 220.00,
            'ready_date' => '2019-11-05',
            'quantity' => 40,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Tree Tomatoes',
            'image' => 'treetomato.webp',
            'price' => 200.00,
            'ready_date' => '2019-11-05',
            'quantity' => 38,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $fruitsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Strawberries',
            'image' => 'strawberry.webp',
            'price' => 250.00,
            'ready_date' => '2019-11-05',
            'quantity' => 40,
            'description'=> 'Fresh produce anytime you need it'
        ]);

        Product::create([
            'category_id' => $cerealsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Garden Peas (shelled)',
            'image' => 'gardenpea.webp',
            'price' => 450.00,
            'ready_date' => '2019-11-05',
            'quantity' => 30,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $cerealsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Chick Peas',
            'image' => 'chickpea.webp',
            'price' => 200.00,
            'ready_date' => '2019-11-05',
            'quantity' => 45,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $cerealsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'French Beans (miciri)',
            'image' => 'frenchbean.webp',
            'price' => 120.00,
            'ready_date' => '2019-11-05',
            'quantity' => 22,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $cerealsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Garden Peas (unshelled)',
            'image' => 'gardenpeaunshelled.webp',
            'price' => 250.00,
            'ready_date' => '2019-11-05',
            'quantity' => 71,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $cerealsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Sugar Snaps',
            'image' => 'sugarsnap.webp',
            'price' => 130.00,
            'ready_date' => '2019-11-05',
            'quantity' => 24,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $cerealsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Baby Corn',
            'image' => 'babycorn.webp',
            'price' => 132.00,
            'ready_date' => '2019-11-05',
            'quantity' => 150,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $cerealsCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Green Maize',
            'image' => 'greenmaize.webp',
            'price' => 150.00,
            'ready_date' => '2019-11-05',
            'quantity' => 250,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Cauliflower',
            'image' => 'Cauliflower.webp',
            'price' => 150.00,
            'ready_date' => '2019-11-05',
            'quantity' => 100,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'White cabbage',
            'image' => 'whitecabbage.webp',
            'price' => 80.00,
            'ready_date' => '2019-11-05',
            'quantity' => 80,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Amaranthus (Terere)',
            'image' => 'amaranthus.webp',
            'price' => 130.00,
            'ready_date' => '2019-11-05',
            'quantity' => 24,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Nightshade (Managu)',
            'image' => 'nightshade.webp',
            'price' => 89.00,
            'ready_date' => '2019-11-05',
            'quantity' => 90,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Cucumber',
            'image' => 'cucumber.webp',
            'price' => 112.00,
            'ready_date' => '2019-11-05',
            'quantity' => 45,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'White Onions',
            'image' => 'whiteonion.webp',
            'price' => 200.00,
            'ready_date' => '2019-11-05',
            'quantity' => 42,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Lettuce (iceberg)',
            'image' => 'lettuce.webp',
            'price' => 83.00,
            'ready_date' => '2019-11-05',
            'quantity' => 86,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Red cabbage',
            'image' => 'redcabbage.webp',
            'price' => 120.00,
            'ready_date' => '2019-11-05',
            'quantity' => 56,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Onions',
            'image' => 'onion.webp',
            'price' => 146.00,
            'ready_date' => '2019-11-05',
            'quantity' => 170,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Kales',
            'image' => 'kale.webp',
            'price' => 80.00,
            'ready_date' => '2019-11-05',
            'quantity' => 89,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Egg plant',
            'image' => 'eggplant.webp',
            'price' => 100.00,
            'ready_date' => '2019-11-05',
            'quantity' => 100,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Spinach',
            'image' => 'spinach.webp',
            'price' => 89.00,
            'ready_date' => '2019-11-05',
            'quantity' => 56,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Celery',
            'image' => 'celery.webp',
            'price' => 150.00,
            'ready_date' => '2019-11-05',
            'quantity' => 74,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Celery',
            'image' => 'celery.webp',
            'price' => 150.00,
            'ready_date' => '2019-11-05',
            'quantity' => 175,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Broccoli',
            'image' => 'brocolli.webp',
            'price' => 150.00,
            'ready_date' => '2019-11-05',
            'quantity' => 165,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Bitter Gourd',
            'image' => 'bittergourd.webp',
            'price' => 150.00,
            'ready_date' => '2019-11-05',
            'quantity' => 100,
            'description'=> 'Fresh produce anytime you need it'
        ]);
         Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Tomatoes',
            'image' => 'tomato.webp',
            'price' => 120.00,
            'ready_date' => '2019-11-05',
            'quantity' => 85,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Cucumber',
            'image' => 'cucumber.webp',
            'price' => 130.00,
            'ready_date' => '2019-11-05',
            'quantity' => 75,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Chives',
            'image' => 'chive.webp',
            'price' => 35.00,
            'ready_date' => '2019-11-05',
            'quantity' => 72,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Courgette (Zucchini)',
            'image' => 'courgette.webp',
            'price' => 97.00,
            'ready_date' => '2019-11-05',
            'quantity' => 126,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Green Capsicum (hoho)',
            'image' => 'greencupsicum.webp',
            'price' => 148.00,
            'ready_date' => '2019-11-05',
            'quantity' => 569,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Pumpkin',
            'image' => 'pumpkin.webp',
            'price' => 200.00,
            'ready_date' => '2019-11-05',
            'quantity' => 265,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Chinese cabbage',
            'image' => 'chinesecabbage.webp',
            'price' => 200.00,
            'ready_date' => '2019-11-05',
            'quantity' => 42,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Yellow/red capsicum (hoho)',
            'image' => 'yellowredcupsicum.webp',
            'price' => 200.00,
            'ready_date' => '2019-11-05',
            'quantity' => 93,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Green Bananas',
            'image' => 'greenbanana.webp',
            'price' => 150.00,
            'ready_date' => '2019-11-05',
            'quantity' => 165,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Pumpkin Leaves',
            'image' => 'pumpkinleaf.webp',
            'price' => 130.00,
            'ready_date' => '2019-11-05',
            'quantity' => 65,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Leeks',
            'image' => 'leek.webp',
            'price' => 130.00,
            'ready_date' => '2019-11-05',
            'quantity' => 125,
            'description'=> 'Fresh produce anytime you need it'
        ]);
        Product::create([
            'category_id' => $vegetablesCategory->id,
            'user_id' => $sellers->random()->id,
            'name' => 'Lettuce (Soft/Green)',
            'image' => 'lettuce.webp',
            'price' => 35.00,
            'ready_date' => '2019-11-05',
            'quantity' => 63,
            'description'=> 'Fresh produce anytime you need it'
        ]);
    }
}
