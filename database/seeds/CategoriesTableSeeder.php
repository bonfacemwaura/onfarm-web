<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Fruits',
            'image' => 'fruits.jpg'
        ]);

        Category::create([
            'name' => 'Vegetables',
            'image' => 'vegetables.jpg'
        ]);

        Category::create([
            'name' => 'Animal Products',
            'image' => 'animal_products.jpg'
        ]);

        Category::create([
            'name' => 'Cereals',
            'image' => 'cereals.jpg'
        ]);
    }
}
