<?php

use App\Category;
use App\Product;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create admins
        factory(User::class, 2)
        ->create()
        ->each(function($user) {
            $user->assignRole('admin');
        });

        // Create sellers
        $categories = Category::all()->toArray();

        factory(User::class, 50)
            ->create()
            ->each(function($user) use ($categories) {
                $user->assignRole('seller');
            });

        // Create buyers
        factory(User::class, 100)
            ->create()
            ->each(function($user) {
                $user->assignRole('buyer');
            });

    }
}
