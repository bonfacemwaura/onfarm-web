@extends('layouts.landing')

@section('styles')

@endsection
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@section('content')

<div class="hero-wrap hero-bread" style="background-image: url('img/bg_1.jpg');">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Cart</span></p>
        <h1 class="mb-0 bread">My Cart</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section ftco-cart">
  <div class="container">
    <div class="row">
      <div class="col-md-12 ftco-animate">
        <div class="cart-list" style="overflow:hidden;">
          <table class="table">
            <thead class="thead-primary">
              <tr class="text-center">
                <table id="cart" class="table table-hover table-condensed">
                  <tr>
                    <th style="width:50%">Product</th>
                    <th style="width:10%">Price</th>
                    <th style="width:8%">Quantity</th>
                    <th style="width:22%" class="text-center">Subtotal</th>
                    <th style="width:10%"></th>
                  </tr>
            </thead>
            <tbody>

              <?php $total = 0 ?>
              @foreach((array) session('cart') as $id => $details)
              <?php $total += $details['price'] * $details['quantity'] ?>
              @endforeach

              @if(session('cart'))
              @foreach(session('cart') as $id => $details)

              <?php $total += $details['price'] * $details['quantity'] ?>

              <tr>
                <td data-th="Product">
                  <div class="row">

                    <div class="col-sm-3 hidden-xs"><img src="{{asset('img/products/'. $details['image']) }}"
                        width="100" height="100" class="img-responsive" /></div>
                    <div class="col-sm-9">
                      <h4 class="nomargin">{{ $details['name'] }}</h4>
                    </div>
                  </div>
                </td>
                <td data-th="Price">ksh {{ $details['price'] }}</td>
                <td data-th="Quantity">
                  <input type="number" data-id="{{ $id }}" data-price="{{$details['price']}}"
                    value="{{ $details['quantity'] }}" class="form-control quantity" />
                </td>
                <td id="subtotal-{{$id}}" data-th="Subtotal" class="text-center subtotal">Ksh
                  {{ $details['price'] * $details['quantity'] }}</td>
                <td class="actions" data-th="">
                  <button class="btn btn btn-sm remove-from-cart" data-id="{{ $id }}"><i
                      class="fa fa-trash"></i></button>
                </td>
              </tr>
              @endforeach
              @endif
            </tbody>
            <tfoot>
              <tr class="visible-xs">
              </tr>
              <tr>
                <td colspan="2" class="hidden-xs"></td>
            </tfoot>
          </table>

          <div class="col-lg-4 mt-5 cart-wrap ftco-animate">
            <div class="cart-total mb-3">
              <h3>Cart Totals</h3>
              <p class="d-flex">
                <span>Total</span>
                <span id="cart-total-price">{{$total/2}}</span>
              </p>

              <p><a tabindex="-1" id="confirm-order" class="btn btn-primary py-3 px-4">Confirm order</a></p>

            </div>
          
            

</section>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script>
  $(".quantity").change(function(e) {
    var ele = $(this);
    $.ajax({
      url: '{{ url('update-cart') }}',
      method: "patch",
      data: {_token: '{{ csrf_token() }}', 
      id: ele.attr("data-id"), 
      quantity: ele.val()
    },
    success: function() {
      // Update subtotal
      $("#subtotal-" + ele.attr("data-id")).html("Ksh " + (ele.val() * ele.attr("data-price")));

      // Update grand total
      let total = 0;
      $(".subtotal").each(function() {
        let price = Number((($(this).html()).split("Ksh")[1]).trim());
        total += price;
      });
      $("#cart-total-price").html(total);
    }});
  });

  $(".remove-from-cart").click(function(e) {
    e.preventDefault();
    var ele = $(this);
    if(confirm("Are you sure")) {
      $.ajax({
        url: '{{ url('remove-from-cart') }}',
        method: "DELETE",
        data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
        success: function (response) {
        window.location.reload();
        }
      });
    }
  });

  $("#confirm-order").click(function(e) {
    e.preventDefault();
    $.ajax({
      url: "{{ url('make-order') }}",
      method: "POST",
      data: {
        _token: '{{ csrf_token() }}',
        total: parseFloat($("#cart-total-price").html()).toFixed(2)
      },
      success: function() {
        Swal.fire({
          title: 'Order Confirmed!',
          text: 'Your order has been confirmed. Thank you for shopping with Onfarm',
          type: 'success',
          confirmButtonText: 'Ok'
        }).then(function() {
          window.location.href = "{{ route('front.products') }}";
        });
      },
      error: function() {
        Swal.fire({
          title: 'Oops!',
          text: 'Your order has could not be confirmed. Please try again.',
          type: 'error',
          confirmButtonText: 'Ok'
        });
      }
    });
  });

</script>
@endsection