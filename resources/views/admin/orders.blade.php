@extends('admin.layout.admin')
@section('content')
    <h3>Orders</h3>
    <hr>

    <ul>
        @foreach($orders as $order)
            <li>
                <h4>Order by {{$order->user->name}} <br> Total Price: {{$order->total_price}}</h4>
                <p>Customer phone number: {{$order->user->phone_number}}</p>
                <h5>Items</h5>
                <table class="table table-bordered">
                    <tr>
                        <th>Name</th>
                        <th>quantity</th>
                        <th>price</th>
                    </tr>
                    @foreach($order->orderItems as $item)
                        <tr>
                            <td>{{$item->name}}</td>
                            <td>{{$item->pivot->quantity}}</td>
                            <td>{{$item->pivot->total}}</td>
                        </tr>

                    @endforeach
                </table>
                <form action="{{route('toggle.deliver',$order->id)}}" method="POST" class="pull-right" id="deliver-toggle">
                    {{csrf_field()}}
                    <label for="delivered">Delivered</label>
                    <input type="checkbox" value="1" name="delivered"  {{$order->delivered==1?"checked":"" }}>
                    <input type="submit" value="Submit">
                </form>

                <div class="clearfix"></div>
                <hr>
                
                
            </li>

        @endforeach

    </ul>
@endsection

