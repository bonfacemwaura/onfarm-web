<!DOCTYPE html>
<html lang="en">

<head>
  <title>Onfarm</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap"
    rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('css/open-iconic-bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
  <link rel="stylesheet" href="{{ asset('css/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('css/jquery.timepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('css/flaticon.css') }}">
  <link rel="stylesheet" href="{{ asset('css/icomoon.css') }}">
  <link rel="stylesheet" href="{{ asset('css/style.css') }}">
  
  @yield('styles')
</head>

<body class="goto-here">
<div class="container">
  
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title">Reviews</h4>
</div>
<div class="modal-body">
          {!! Form::open(['route' => 'review.store', 'method' => 'POST', 'files' => true, 'data-parsley-validate'=>'']) !!}

            <div class="form-group">
                {{ Form::label('Rating', 'Rating') }}
                {{ Form::number('rating', null, array('class' => 'form-control','required'=>'','max'=>'5')) }}
            </div>
            <div class="form-group">
                {{ Form::label('Heading', 'Heading') }}
                {{ Form::text('headline', null, array('class' => 'form-control')) }}
            </div>
            
            <div class="form-group">
                {{ Form::label('Tell us more', 'description') }}
                {{ Form::text('description', null, array('class' => 'form-control')) }}
            </div>
            
            <div class="form-group">
                
                {{ Form::hidden('product_id', $product->id)}}
            </div>
           
            

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          {{ Form::submit('Submit', array('class' => 'btn btn-default')) }}
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>
</div>






</body>
</html>

        
          