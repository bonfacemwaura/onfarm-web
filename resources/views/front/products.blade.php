@extends('layouts.landing')

@section('content')
<div class="hero-wrap hero-bread" style="background-image: url('img/bg_1.jpg');">
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center">
                <p class="breadcrumbs"><span class="mr-2"><a href="index.html"></a></span> <span></span></p>
                <h1 class="mb-0 bread">Products</h1>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                Search Products
            </div>
            <div class="col-md-3 mb-5 text-center">
                <input class="form-control" type="text" id="product_name" placeholder="Product Name">
            </div>
            <div class="col-md-3 mb-5 text-center">
                <input class="form-control" type="number" id="product_quantity" placeholder="Product Quantity">
            </div>
            <div class="col-md-3 mb-5 text-center">
                <input class="form-control" type="date" id="product_ready_date" placeholder="Ready Date">
            </div>
            <div class="col-md-3 mb-5 text-center">
                <button id="search-products" style="padding: 13px 0px;" class="btn btn-primary btn-block">Search</button>
            </div>

            <div class="col-md-10 mb-5 text-center">
                <ul class="product-category">
                <li><a href="{{route('front.products')}}" class="{{app('request')->has('category') ? '' : 'active'}}">All</a></li>
                    @foreach($categories as $category)
                    <li><a id="{{$category->id}}" href="{{route('front.products', ['category' => $category->id ])}}" 
                        class="{{app('request')->has('category') ? app('request')->input('category') == $category->id ? 'active' : '' : ''}}">{{$category->name}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="row">
            @if(count($products) > 0)
            @foreach($products as $product)
            <div class="col-md-6 col-lg-3 ftco-animate">
                <div class="product justify-content-center">
                    <a href="{{route('product',$product->id)}}"class="img-prod" class="img-responsive"><img class="img-fluid product__image" src="{{asset('img/products/' . $product->image )}}" alt="OnFarm Product">

                        <div class="overlay"></div>
                    </a>
                    <div class="text py-3 pb-4 px-3 text-center">
                        <h3><a href="#">{{$product->name}}</a></h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span class="price-sale">KSH.{{$product->price}}</span></p>

                            </div>
                        </div>
                        <div class="bottom-area d-flex px-3">
                            <div class="m-auto d-flex">
                                <a href="{{ url('add-to-cart/'.$product->id) }}"
                                    class="buy-now d-flex justify-content-center align-items-center mx-1">
                                    <span><i class="ion-ios-cart"></i></span>
                                </a>
                               
                                </a>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            @endforeach
            @else
            <div class="col-md-12 text-center mt-5">
                <p>No Products Found</p>
            </div>
            @endif
        </div>
        <div class="row mt-5">
            <div class="col text-center">
                <div class="block-27">
                     {{ $products->appends(request()->query())->links() }} 
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script>
 // Products filter
 $("#search-products").click(function(e) {
    e.preventDefault();

    let name = $("#product_name").val();
    let quantity = $("#product_quantity").val();
    let readyDate = $("#product_ready_date").val();
    let querySymbol = "?";
    let url = "{{route('front.products')}}";

    if (name.length > 0) {
        url += querySymbol + "name=" + name;
        querySymbol = "&";
    } 

    if (quantity.length > 0) {
        url += querySymbol + "quantity=" + quantity;
        querySymbol = "&";
    } 

    if (readyDate.length > 0) {
        url += querySymbol + "ready_date=" + readyDate;
        querySymbol = "&";
    } 

    window.location.href = url;
 });
</script>
@endsection

