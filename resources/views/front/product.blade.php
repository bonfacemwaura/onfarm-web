@extends('layouts.landing')

@section('content')

	
    <div class="hero-wrap hero-bread" style="background-image: url({{asset('img/bg_1.jpg')}});">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<p class="breadcrumbs"><span class="mr-2"><a href="index.html"></a></span> <span class="mr-2"><a href="#"></a></span> <span></span></p>
            <h1 class="mb-0 bread">{{$product->category->name  }}</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section">
    	<div class="container">
    		<div class="row">
    			<div class="col-lg-6 mb-5 ftco-animate">
    				<a href="#" class="image-popup"><img src="{{asset('img/products/' . $product->image )}}" class="img-fluid" alt="Onfarm"></a>
    			</div>
    			<div class="col-lg-6 product-details pl-md-5 ftco-animate">
            <h3>{{$product->name}}-{{$product->user->name}} </h3>
            <p>Seller Phone Number: {{$product->user->phone_number}}</p>
           <p>Ready on {{$product->ready_date}}</p>
           <p>Quantity: {{$product->quantity}} kgs</p>
            <h5><u>About product</u></h5>
           <h5>{{$product->description}}</h5>
            <p class="price" style = "color:green"><span>ksh{{$product->price}} per kg</span></p>
            <p><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-black py-3 px-5">Add to Cart</a></p>
          
          </div>
    		</div>
    	</div>
    </section>

    <div class="item-wrapper">
      

<star-rating :rating="{{$product->getStarRating()}}"></star-rating>
<br>
<div class="item-wrapper">
<ul>
<div class="col-lg-4 mt-5 cart-wrap">
            <div class="cart-total mb-3">
              <h2>Product Reviews</h2>
            @forelse($product->reviews as $review)

<li>
    {{$review->headline}} :from
    {{$review->user->name}}-
    Rating: {{$review->rating}}
</li>


@empty

@endforelse
            </div>

<div>
@if(auth()->check())
<button type="button" class= "btn btn-black py-3 px-5" data-toggle="modal" data-target="#myModal">Write a review</button>


@include('admin.product.partials.review_form')
@else
    <a href="/login" class="button" >Write a review</a>

@endif
</div>


</ul>

</div>
</div>
</div>
<star-rating></star-rating>
@endsection



