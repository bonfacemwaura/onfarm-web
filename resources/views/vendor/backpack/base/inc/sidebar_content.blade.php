<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li><a href="{{ backpack_url('order') }}"><i class="fa fa-tag"></i> <span>View orders</span></a></li>
<li><a href="{{ backpack_url('category') }}"><i class="fa fa-tag"></i> <span>Categories</span></a></li>

